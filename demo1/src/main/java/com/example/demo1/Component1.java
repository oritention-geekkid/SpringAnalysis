package com.example.demo1;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Component1 {
    @EventListener
    public void myEventListener(MyEvent myEvent) {
        System.out.println("接收到MyEvent");
    }
}
