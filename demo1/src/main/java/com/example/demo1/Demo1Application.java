package com.example.demo1;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultSingletonBeanRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Locale;
import java.util.Map;

@SpringBootApplication
public class Demo1Application {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, IOException {
        ConfigurableApplicationContext cac = SpringApplication.run(Demo1Application.class, args);

        /* BeanFactory 是 Spring 的最核心容器，主要的 ApplicationContext 实现都组合了它的功能，BeanFactory 实际上只有 getBean() 一个方法，控制反转、依赖注入等其他功能是通过一个个实现类来补充的。 */
        /* ApplicationContext 的 getBean() 方法也调用的是 BeanFactory 的 getBean() 方法。*/
        // cac.getBean("name");

        /* 下面的代码通过反射将 BeanFactory 中的所有单例Bean存入一个Map中 */
        ConfigurableListableBeanFactory beanFactory = cac.getBeanFactory();
        Field singletonObjects = DefaultSingletonBeanRegistry.class.getDeclaredField("singletonObjects");
        singletonObjects.setAccessible(true);
        Map<String, Object> singletonObjectsMap =
                (Map<String, Object>) singletonObjects.get(beanFactory);
        /* 以下结果可以验证上面代码有效（可以看到列出了自定义的两个组件 Component1 和 Component2） */
        singletonObjectsMap.entrySet().stream()
                .filter(e->e.getKey().startsWith("component"))
                .forEach(e->System.out.println(e.getKey()+"="+e.getValue()));

        /* ApplicationContext 比 BeanFactory 多点啥 */
        // 1. 获取对应语言的信息（需自行定义在resources文件夹中）（MessageSource接口）
        System.out.println(cac.getMessage("hi", null, Locale.CHINA));
        System.out.println(cac.getMessage("hi", null, Locale.ENGLISH));

        // 2. 获取Resources（ResourcePatternResolver接口)
        System.out.println(cac.getResource("classpath:application.properties"));
        Resource[] resources = cac.getResources("classpath*:META-INF/spring.factories");
        for (Resource resource : resources) {
            System.out.println(resource);
        }

        // 3. 获取当前程序配置文件中的变量和系统环境变量（EnvironmentCapable接口）
        System.out.println(cac.getEnvironment().getProperty("server.port"));
        System.out.println(cac.getEnvironment().getProperty("java_home"));

        // 4. 发布事件，需要定义Event类（此例定义在MyEvent类中）和EventListener方法（此例定义在Component1类中）（ApplicationEventPublisher接口）
        cac.publishEvent(new MyEvent(cac));

    }

}
