# SpringAnalysis

#### 概述
自己跟着黑马程序员的课对于Spring框架进行原理分析时所用到的demo代码库，笔记也包含在代码注释中。

#### 对各个Module的介绍
demo1 -- BeanFactory和ApplicationContext的功能

demo2 -- 通过BeanFactory实现ApplicationContext的功能，以及通过xml配置文件或Java配置类来加载ApplicationContext

demo3 -- 通过后处理器接口对各个生命周期进行功能增强，通过模板方法模式实现Bean后处理器

demo4 -- 从@Autowired解析机制看Bean后处理器的执行

demo5 -- 从@ComponentScan解析机制看BeanFactory后处理器的执行