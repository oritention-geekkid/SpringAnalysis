package com.example.demo3;

import java.util.ArrayList;
import java.util.List;

public class TemplateMethodPatternTest {
    public static void main(String[] args) {
        MyBeanFactory myBeanFactory = new MyBeanFactory();
        myBeanFactory.addBeanPostProcessor(() -> System.out.println("解析@Autowired"));
        myBeanFactory.addBeanPostProcessor(() -> System.out.println("解析@Resource"));
        myBeanFactory.getBean();
    }

    // TemplateMethodPattern实现后处理器对Bean各个生命周期的功能增强
    static class MyBeanFactory {
        public Object getBean() {
            Object bean = new Object();
            System.out.println("构造 "+bean);
            System.out.println("依赖注入 "+bean);
            for (BeanPostProcessor processor : processors) {
                processor.inject();
            }
            System.out.println("初始化 "+bean);

            return bean;
        }

        private final List<BeanPostProcessor> processors = new ArrayList<>();

        public void addBeanPostProcessor(BeanPostProcessor postProcessor) {
            processors.add(postProcessor);
        }
    }

    interface BeanPostProcessor {
        void inject(); // 对依赖注入阶段的扩展
    }

}
