package com.example.demo4;

import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.StandardEnvironment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class DigInAutowired {
    // Bean后处理器右很多种，此处对@Autowired注解的处理做分析
    public static void main(String[] args) throws Throwable {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.registerSingleton("bean2", new Bean2());

        beanFactory.setAutowireCandidateResolver(new ContextAnnotationAutowireCandidateResolver());  // 处理Value注解

        beanFactory.addEmbeddedValueResolver(new StandardEnvironment()::resolvePlaceholders);  // 解析${}（环境变量）

        // 1.查找被添加@Autowired注解的成员，获取InjectionMetadata
        AutowiredAnnotationBeanPostProcessor processor = new AutowiredAnnotationBeanPostProcessor();
        processor.setBeanFactory(beanFactory);

        Bean1 bean1 = new Bean1();
//        System.out.println(bean1);
//        processor.postProcessProperties(null,bean1,"bean1");  // 执行依赖注入
//        System.out.println(bean1);

        Method findAutowiringMetadata = AutowiredAnnotationBeanPostProcessor.class.getDeclaredMethod("findAutowiringMetadata", String.class, Class.class, PropertyValues.class);
        findAutowiringMetadata.setAccessible(true);
        InjectionMetadata injectionMetadata = (InjectionMetadata) findAutowiringMetadata.invoke(processor, "bean1", Bean1.class, null);  // 获取Bean1上加了@Autowired @Value的成员

        // 2.使用InjectionMetadata的inject方法进行依赖注入
        injectionMetadata.inject(bean1, "bean1", null);

        System.out.println(bean1);

        // 3.根据类型查找值，使用DependencyDescriptor
        Field b2 = Bean1.class.getDeclaredField("bean2");
        DependencyDescriptor dd2 = new DependencyDescriptor(b2,false);
        Object o = beanFactory.doResolveDependency(dd2,null,null, null);
        System.out.println(o);

        Method setHome = Bean1.class.getMethod("setHome", String.class);
        DependencyDescriptor ddSetHome = new DependencyDescriptor(new MethodParameter(setHome,0), false);
        Object oo = beanFactory.doResolveDependency(ddSetHome,null,null,null);
        System.out.println(oo);
    }
}
