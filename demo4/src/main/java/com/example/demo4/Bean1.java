package com.example.demo4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Bean1 {
    String home;

    @Autowired
    private Bean2 bean2;

    @Autowired
    public void setHome(@Value("${JAVA_HOME}") String home) {
        this.home = home;
    }

    @Override
    public String toString() {
        return "Bean2: "+bean2+", JAVA_HOME:"+home;
    }
}
